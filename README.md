# flaubertagger

FlauberTagger is a morphological tagger for French based on Flaubert language model.

## Install the tagger:

* Install anaconda or miniconda (follow instructions at [https://docs.anaconda.com/free/miniconda/index.html](https://docs.anaconda.com/free/miniconda/index.html).
* Run the following commands:

```
conda create -n tagger python=3.8
conda activate tagger
pip install torch transformers==4.6.0 six
```

## Run the tagger

```
cd flaubertagger/src
python tagger.py eval gsd_base_0.00001_0.5_8_aug <input>  <output>   [--cuda 0]
```

where:

- `gsd_base_0.00001_0.5_8_aug` is the path to the unzipped pretrained model
- `input` = path text file with 1 sentence per line. Sentences should be tokenized (e.g. with Stanza) 
- `output` = path to output file that will be created
- `[--cuda]` use this option if you have a GPU available
