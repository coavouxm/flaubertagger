from template import read_forms, get_cases 
#, read_lexique
from collections import defaultdict

unk_cases="""fut.1pl
pst.1pl
pst.2pl
pst.2sg
pst.3pl
sbjv.prs.1pl
sbjv.ipfv.1pl
sbjv.ipfv.1sg
sbjv.ipfv.2pl
sbjv.ipfv.2sg
sbjv.ipfv.3pl
sbjv.ipfv.3sg""".split("\n")

rare_cases="""cond.1pl
cond.2pl
cond.2sg
cond.3pl
fut.3pl
pst.1sg""".split("\n")


#relevant_cases = set(unk_cases + rare_cases)
relevant_cases = ["sbjv.prs.2sg", "sbjv.prs.2pl", "sbjv.prs.3pl", "sbjv.prs.1sg", "ipfv.1pl", "imp.1pl", "fut.2sg", "fut.2pl"]
print(sorted(relevant_cases))

def main():

    lemme2forms = read_forms("forms.csv")
    listforms = list(lemme2forms.values())
    cases = get_cases()
    
    cases2forms = defaultdict(list)
    
    for l in listforms:
        for c, form in l:
            case = cases[c]
            if case in relevant_cases:
                form = form.split("|")
                for f in form:
                    cases2forms[case].append(f)
    
    for case, value_list in cases2forms.items():
        with open(f"forms_{case}", "w") as f:
            for verbform in value_list:
                f.write(f"{verbform}\n")


if __name__ == "__main__":
    main()

