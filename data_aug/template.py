import random
import logging
from utils import *
from random import getrandbits

random.seed(2837)

def load_names():
    names = []
    with open("prenoms.txt") as f:
        for line in f:
            if "'" in line:
                continue
            line = line.strip()
            line = "-".join([a.capitalize() for a in line.split("-")])
            names.append(line)
    return names

def randbool(p=None):
    if p is None:
        return not getrandbits(1)
    return random.random() < p

def get_cases():
    with open("cases.txt") as f:
        cases = f.readlines()
        cases = [line.strip().split("\t") for line in cases]
        cases = [(l[0], l[2]) for l in cases]
        cases = dict(cases)
    return cases

def read_forms(filename):
    lemme2forms = {}
    with open(filename) as f:
        lines = [l.strip() for l in f]
        
    for line in lines:
        line = line.split("\t")
        if len(line) != 2:
            continue
        if line[1][0] != "V":
            continue
        
        lemme = line[0]
        #Vmii3p-:chassaient;Vmii1s-:chassais;Vmii2s-:chassais;Vmii3s-:chassait;Vmis1s-:chassai;Vmis1p-:chassâmes;Vmpp---:chassant;Vmsi3p-:chassassent;Vmsi2s-:chassasses;Vmsi1s-:chassasse;Vmsi2p-:chassassiez;Vmsi1p-:chassassions;Vmis2s-:chassas;Vmis2p-:chassâtes;Vmsi3s-:chassât;Vmis3s-:chassa;Vmps-pf:chassées;Vmps-sf:chassée;Vmip3p-:chassent;Vmsp3p-:chassent;Vmcp3p-:chasseraient;Vmcp1s-:chasserais;Vmcp2s-:chasserais;Vmcp3s-:chasserait;Vmif1s-:chasserai;Vmif2s-:chasseras;Vmif3s-:chassera;Vmis3p-:chassèrent;Vmif2p-:chasserez;Vmcp2p-:chasseriez;Vmcp1p-:chasserions;Vmif1p-:chasserons;Vmif3p-:chasseront;Vmn----:chasser;Vmip2s-:chasses;Vmps-pm:chassés;Vmsp2s-:chasses;Vmip1s-:chasse;Vmip3s-:chasse;Vmmp2s-:chasse;Vmps-sm:chassé;Vmsp1s-:chasse;Vmsp3s-:chasse;Vmip2p-:chassez;Vmmp2p-:chassez;Vmii2p-:chassiez;Vmsp2p-:chassiez;Vmii1p-:chassions;Vmsp1p-:chassions;Vmip1p-:chassons;Vmmp1p-:chassons
        
        forms = [form.split(":") for form in line[1].split(";")]
        lemme2forms[lemme] = forms

    return lemme2forms

def read_lexique(filename):
    lemmas = set()
    with open(filename) as f:
        f.readline()
        for line in f:
            line = line.split("\t")
            if line[3] in {"VER", "AUX"}:
                lemmas.add(line[2])
    return lemmas

def decompose_case(case):
    feats = {}
    for k, v in FEATS_FUN.items():
        value = v(case)
        if value is not None:
            feats[k] = value
    return feats

def add_subj_pronoun(feats, sentence):
    # ~ if "Mood" not in feats:
        # ~ return sentence
    if  feats["Mood"] not in {"Cnd", "Sub", "Ind"}:
        return sentence
    p = feats["Person"]
    n = feats["Number"]
    
    #PRON	_	Number=Sing|Person=1|PronType=Prs
    form = random.choice(SUBJ_CLITICS[(p, n)])
    
    return [(form, "PRON", SUBJ_CLITICS_FEATS[form])] + sentence

def add_obj_pronoun(feats, sentence, negation):
    if randbool():
        return sentence
    
    if feats["Mood"] == "Imp" and not negation:
        form = random.choice(S_CLITICS[(feats["Person"], feats["Number"])])
        cl_feats = OBJ_CLITICS_FEATS[form]
        sentence.append((f"-{form}", "PRON", cl_feats))
        return sentence
        # ~ if negation:
            # ~ form = random.choice(S_CLITICS[(feats["Person"], feats["Number"])])
            # ~ cl_feats = OBJ_CLITICS_FEATS[form]
            # ~ return [(f"-{form}", "PRON", cl_feats)] + sentence

    CLITICS = S_CLITICS if feats["Mood"] == "Imp" else W_CLITICS

    form = random.choice(CLITICS[(feats["Person"], feats["Number"])])
    cl_feats = OBJ_CLITICS_FEATS[form]
    return [(form, "PRON", cl_feats)] + sentence

    # ~ form = random.choice(W_CLITICS)
    # ~ cl_feats = OBJ_CLITICS_FEATS[form]
    # ~ return [(form, "PRON", cl_feats)] + sentence

def add_negation(features, sentence):
    #15	n'	ne	ADV	_	Polarity=Neg	16	advmod	_	SpaceAfter=No
    #8	pas	pas	ADV	_	Polarity=Neg	9	advmod	_	_
    return  [("ne", "ADV", "Polarity=Neg")] + sentence + [("pas", "ADV", "Polarity=Neg")]

def generate_sentence(sentence):
    vowels = "aeiouyéèàâêîôûäëïöüh"

    for i, (token, POS, feats) in enumerate(sentence):
        if i == 0 and randbool(0.8):
            token = "-".join([a.capitalize() for a in token.split("-")])
        if token.lower() in {"je", "que", "me", "te", "le", "la", "ne"}:
            if i+1 < len(sentence) and sentence[i+1][0][0] in vowels:
                token = token[:-1] + "'"
        
        conll_token = [f"{i+1}", token, "_", POS, "_", feats, "_", "_", "_", "_"]
        print("\t".join(conll_token))

def generate_main_clause_for_sub(tense, negation):
    verb = [("faut", "VERB", "Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin")]
    if tense == "Imp":
        verb = [("fallait", "VERB", "Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin")]
    if negation:
        verb = [("ne", "ADV", "Polarity=Neg")] + verb + [("pas", "ADV", "Polarity=Neg")]
    return [("il", "PRON", "Gender=Masc|Number=Sing|Person=3|PronType=Prs")] + verb + [("que", "SCONJ", "_")]

def generate_template(case, form, names):
    
    #print(case, form)
    feats = decompose_case(case)
    feats_str = "|".join([f"{k}={v}" for k, v in sorted(feats.items())])
    
    if "VerbForm" in feats and feats["VerbForm"] in {"Inf", "Part"}:
        logging.warning(f"Ignoring VerbForm={feats['VerbForm']}")
        return
    
    sentence = [(form, "VERB", feats_str)]
    
    negation = randbool(0.2)
    sentence = add_obj_pronoun(feats, sentence, negation)
    if negation and feats["Mood"] != "Sub":
        sentence = add_negation(feats, sentence)
    sentence = add_subj_pronoun(feats, sentence)

    if "Mood" in feats and feats["Mood"] == "Sub":
        sentence = generate_main_clause_for_sub(feats["Tense"], negation) + sentence

    if randbool(0.7):
        punct = ". !".split()
        sentence.append((random.choice(punct), "PUNCT", "_"))

    if feats["Mood"] == "Imp" and feats["Person"] == "2" and randbool(0.5):
        name = random.choice(names)
        sentence = [(name, "PROPN", "_"),
                    (",", "PUNCT", "_")] + sentence

    generate_sentence(sentence)
    print()


def main():
    
    lemme2forms = read_forms("forms.csv")
    lexique = read_lexique("/home/mcoavoux/data/Lexique383/Lexique383.tsv")

    names = load_names()

    #print(f"Number lemmas: {len(lemme2forms)}")
    lemme2forms = {k:v for k, v in lemme2forms.items() if k in lexique}
    #print(f"Number filtered lemmas: {len(lemme2forms)}")

    listforms = list(lemme2forms.values())
    random.shuffle(listforms)
    
    cases = get_cases()
    
    for lemmeforms in listforms:
        for case, form in lemmeforms:
            
            if "|" in form:
                form = random.choice(form.split("|"))
            
            generate_template(cases[case], form, names)


if __name__ == "__main__":
    main()
