
def get_person(case):
    if "1" in case: return "1"
    if "2" in case: return "2"
    if "3" in case: return "3"
    return None

def get_number(case):
    if "sg" in case: return "Sing"
    if "pl" in case: return "Plur"
    return None

def get_tense(case):
    if "prs" in case: return "Pres"
    if "imp" in case: return "Pres"
    if "cond" in case: return "Pres"
    if "fut" in case: return "Fut"
    if "pst" in case: return "Past"
    if "ipfv" in case: return "Imp"

def get_verbform(case):
    if "ptcp" in case: return "Part"
    if "inf" in case: return "Inf"
    return "Fin"

def get_mood(case):
    if "cond" in case: return "Cnd"
    if "imp" in case: return "Imp"
    if "sbjv" in case: return "Sub"
    if "ptcp" in case: return None
    if "inf" in case: return None
    return "Ind"

def get_gender(case):
    if ".f." in case: return "Fem"
    if ".m." in case: return "Masc"
    return None

FEATS_FUN = {"Person": get_person,
             "Number": get_number,
             "Gender": get_gender,
             "Tense": get_tense,
             "VerbForm": get_verbform,
             "Mood" : get_mood}
            

SUBJ_CLITICS = {("1", "Sing"): ["je"],
                ("2", "Sing"): ["tu"],
                ("3", "Sing"): ["il", "elle", "on", "iel"],
                ("1", "Plur"): ["nous"],
                ("2", "Plur"): ["vous"],
                ("3", "Plur"): ["ils", "elles", "iels"]}

SUBJ_CLITICS_FEATS = {
    "je":   "Number=Sing|Person=1|PronType=Prs",
    "tu":   "Number=Sing|Person=2|PronType=Prs", 
    "il":   "Gender=Masc|Number=Sing|Person=3|PronType=Prs",
    "elle": "Gender=Fem|Number=Sing|Person=3|PronType=Prs",
    "on":   "Gender=Masc|Number=Sing|Person=3",
    "iel":  "Number=Sing|Person=3|PronType=Prs",
    "nous": "Number=Plur|Person=1|PronType=Prs",
    "vous": "Number=Plur|Person=2|PronType=Prs",
    "ils":  "Gender=Masc|Number=Plur|Person=3|PronType=Prs",
    "elles":"Gender=Fem|Number=Plur|Person=3|PronType=Prs",
    "iels": "Number=Plur|Person=3|PronType=Prs"
}

OBJ_CLITICS_FEATS = {
    "me": "Number=Sing|Person=1|PronType=Prs",
    "nous": "Number=Plur|Person=1|PronType=Prs",
    "te": "Number=Sing|Person=2|PronType=Prs",
    "vous": "Number=Plur|Person=2|PronType=Prs",
    "le": "Gender=Masc|Number=Sing|Person=3|PronType=Prs",
    "la": "Gender=Fem|Number=Sing|Person=3|PronType=Prs",
    "les": "Number=Plur|Person=3|PronType=Prs",
    "leur": "Number=Plur|Person=3|PronType=Prs",
    "lui": "Number=Sing|Person=3|PronType=Prs",
    "moi": "Number=Sing|Person=1",
    "toi": "Number=Sing|Person=2",
}

#W_CLITICS = "me nous te vous le la les leur lui".split()
W_CLITICS = {("1", "Sing"): "me nous te vous le la les leur lui".split(),
             ("2", "Sing"): "me nous te le la les leur lui".split(),
             ("3", "Sing"): "me nous te vous le la les leur lui".split(),
             ("1", "Plur"): "nous te vous le la les leur lui".split(),
             ("2", "Plur"): "me nous vous le la les leur lui".split(),
             ("3", "Plur"): "me nous te vous le la les leur lui".split()}

S_CLITICS = {("2", "Sing"): "nous le la les leur lui moi toi".split(),
             ("2", "Plur"): "nous vous le la les leur lui moi".split(),
             ("1", "Plur"): "nous le la les leur lui".split() }



