# Note: has word embeddings, gradient clipping and gaussian noise

#if [ "$#" -le 2 ]
#then
#    echo "illegal number of parameters"
#    echo Usage:
#    echo    "sh expe_master.sh (negra|dptb|tiger_spmrl) modelname *args"
#    exit 1
#fi

source /home/getalp/coavouxm/anaconda3/bin/activate py37cuda9

#git rev-parse HEAD > ${mod}/git_rev
#conda list --explicit > ${mod}/conda-requirements.txt
#pip freeze > ${mod}/requirements.txt


#corpus=$1

#tr=~/data/multilingual_disco_data/data/${corpus}/train.discbracket
#dev=~/data/multilingual_disco_data/data/${corpus}/dev.discbracket

#dtok=~/data/multilingual_disco_data/data/${corpus}/dev.tokens
#ttok=~/data/multilingual_disco_data/data/${corpus}/test.tokens

#tgold=~/data/multilingual_disco_data/data/${corpus}/test.discbracket


#shift
#shift


#args="${tr} ${dev} $@"

name=$1

python ../src/tagger.py train $@   > ${name}.out 2> ${name}.err

