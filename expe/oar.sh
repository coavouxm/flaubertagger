

#small="flaubert/flaubert_small_cased"
base="flaubert/flaubert_base_cased"

folder=2020_12_07_expe_gsd_ftb

mkdir -p ${folder}

#~ bert=${small}
#~ batchsize=64
#~ for lr in 0.00001 0.00002 0.00006 0.0001
#~ do
    #~ mod="${folder}/small_${lr}"
    #~ oarsub -l /core=8/gpu=1,walltime=4 "bash expe_master.sh ${mod}  -l ${lr} --cuda 0 --bert-id ${bert}  -i 10 -B ${batchsize}"
#~ done

bert=${base}
batchsize=16
d=0.5
for lr in 0.00001 0.00002
do
    #for dataset in gsd partut sequoia
    for dataset in gsd ftb_spmrl
    do
        mod="${folder}/${dataset}_base_${lr}_${d}_${batchsize}"
        oarsub -l /host=1/core=8/gpu=1,walltime=4 "bash expe_master.sh ${mod} -D ${d} -l ${lr} --cuda 0 --bert-id ${bert}  -i 16 -B ${batchsize} --cpos --corpus ${dataset}"
    done

done
